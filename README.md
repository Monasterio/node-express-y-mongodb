# API Node, Express y mongoDb como prueba de Jose Monasterio para Epayco

## Instalación y ejecución
  Se ejecuta con yarn ó npm
    
    * npm install o yarn install para instalar todas las dependencias.
    
    * configurar el .env a partir del .env.example.
    
    * npm run seed o yarn seed para ejecutar las migración de los datos base.
    
    * npm run dev o yarn dev para ejecutar el Api server.

# Puerto para documentador swagger
  Una vez creado el .env suponiendo que el puerto por defecto para el proyecto sea 9000 se puede ver 
  la documentacion de creada con swagger en la siguiente direccion http://localhost:9000/doc 

## Es necesario tener instalado mongoDb al momento de ejecutar la prueba del codigo.
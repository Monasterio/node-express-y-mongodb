import moongose from 'mongoose';

const invoicetSchema = moongose.Schema(
  {
    id: { type: String, required: true, unique: true },
    code: { type: String, required: true, unique: true },
    subtotal: { type: Number, required: true },
    creator: { type: moongose.Schema.Types.ObjectId, ref: 'User' },
    total: { type: String, required: true },
    status: {
      type: String,
      enum: ['Payment', 'To pay'],
      required: true,
    },
    discount: { type: String, required: true },
    products: [{ type: Object }],
    client: { type: moongose.Schema.Types.ObjectId, ref: 'Client' },
  },
  {
    timestamps: true,
  },
);

const invoice = moongose.model('Invoice', invoicetSchema);
export default invoice;

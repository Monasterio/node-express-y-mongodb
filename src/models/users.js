import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
  {
    id: { type: String, required: true },
    name: { type: String, required: true },
    idReset: { type: String },
    lastName: { type: String, required: true },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    type: {
      type: String,
      enum: ['Client', 'Owner'],
      required: true,
    },
    password: {
      type: String,
      select: false,
      min: 6,
      max: 10,
    },
  },
  {
    timestamps: true,
  },
);

const model = mongoose.model('User', userSchema);

module.exports = model;

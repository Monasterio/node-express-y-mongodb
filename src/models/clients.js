import mongoose from 'mongoose';

const clientSchema = new mongoose.Schema(
  {
    id: { type: String, required: true },
    name: { type: String, required: true },
    phone: { type: String, required: true },
    ci: { type: String, required: true, maxlength: 12, minlength: 6 },
    address: { type: String },
    lastName: { type: String, required: true },
    email: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  },
);

const model = mongoose.model('Client', clientSchema);

module.exports = model;

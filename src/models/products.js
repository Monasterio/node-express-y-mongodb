import moongose from 'mongoose';

const productSchema = moongose.Schema(
  {
    id: { type: String, required: true, unique: true },
    code: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    price: { type: Number, required: true },
    description: { type: String, required: true },
  },
  {
    timestamps: true,
  },
);

const product = moongose.model('Product', productSchema);
export default product;

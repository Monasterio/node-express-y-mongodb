import { Router } from 'express';
import user from '../../controllers/v1/users-controllers';
import Authenticacion from '../../middlewares/authentication';
import validate from '../../middlewares/validate';

const router = Router();

router.post('/create', validate.create, user.create);
router.post('/login', user.login);
router.get('/logout', user.logout);
router.post('/id-reset-password', user.resetPassword);
router.post(
  '/changed-password',
  validate.changedPassword,
  user.changedPassword,
);
router.get('/get-all', Authenticacion.validateUser, user.getAll);
router.delete('/delete/:id', user.deleteUser);
router.get('/get-one/:id', user.getOne);
router.put('/update/:id', user.updateUser);

export default router;

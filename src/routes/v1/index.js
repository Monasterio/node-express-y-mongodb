import userRoutes from './users-routes';
import productRoutes from './products-routers';
import invoiceiRoutes from './invoices-routers';
import clientRoutes from './clients-routes';

export default (app) => {
  app.use('/user/', userRoutes);
  app.use('/client/', clientRoutes);
  app.use('/product/', productRoutes);
  app.use('/invoice/', invoiceiRoutes);
};

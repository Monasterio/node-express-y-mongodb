import { Router } from 'express';
import Controller from '../../controllers/v1/invoices-controllers';

const router = Router();

router.post('/create', Controller.create);
router.get('/get-all', Controller.getAll);
router.get('/pay/:id', Controller.payInvoice);
router.get('/get-one/:id', Controller.getOne);
router.put('/update/:id', Controller.update);
router.delete('/delete/:id', Controller.delete);
export default router;

import { Router } from 'express';
import model from '../../controllers/v1/clients-controllers';
import validate from '../../middlewares/validate';

const router = Router();

router.post('/create', model.create);
router.get('/get-all', model.getAll);
router.delete('/delete/:id', model.delete);
router.get('/get-one/:id', model.getOne);

export default router;

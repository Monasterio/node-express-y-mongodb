import { Router } from 'express';
import Controller from '../../controllers/v1/products-controllers';
import ProductValidate from '../../middlewares/productValidate';

const router = Router();

router.post('/create', ProductValidate.body, Controller.create);
router.get('/get-all', Controller.getAll);
router.get('/get-one/:id', Controller.getOne);
router.put('/update/:id', ProductValidate.body, Controller.update);
router.delete('/delete/:id', Controller.delete);
export default router;

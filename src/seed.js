import seeder from 'mongoose-seed';
import dotenv from 'dotenv';
import { Seeder } from './seeders/index';

dotenv.config();
// Connect to MongoDB via Mongoose
seeder.connect(
  `mongodb://${process.env.DDB_USER}:${process.env.DDB_PASSWORD}@${process.env.DDB_HOST}:${process.env.DDB_PORT}/${process.env.DDB_NAME}`,
  () => {
    seeder.loadModels([
      './src/models/users',
      './src/models/products',
      './src/models/invoices',
    ]);

    // Clear specified collections
    seeder.clearModels(
      [
        'User',
        'Product',
        'Invoice',
      ],
      () => {
        // Callback to populate DB once collections have been cleared
        seeder.populateModels(Seeder, () => {
          seeder.disconnect();
        });
      }
    );
  }
);

// Data array containing seed data - documents organized by Model

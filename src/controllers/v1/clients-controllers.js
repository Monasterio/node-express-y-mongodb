/* eslint-disable class-methods-use-this */
import { v4 as uuid4 } from 'uuid';
import dotenv from 'dotenv';
import DB from '../../database';
import Model from '../../models/clients';

dotenv.config();

class ModelController {
  async create(req, res) {
    DB.connection.then(async () => {
      const uuid = uuid4();
      const model = new Model({
        id: uuid,
        name: req.body.name,
        ci: req.body.ci,
        phone: req.body.phone,
        lastName: req.body.lastName,
        email: req.body.email,
        address: req.body.address,
      });
      await model.save().then(
        (response) => {
          res.send({ status: 'ok', data: response });
        },
        (err) => {
          console.log(err)
          const error = err.errmsg || err.message;
          if (error.indexOf('email_1') !== -1) {
            res.status(400).json({
              status: 'error',
              msg_ing: 'Email already exists',
              msg_esp: 'Email ya esta registrado',
            });
          } else {
            res.status(500).json({
              status: 'error',
              msg_ing: 'Problem with created user',
              msg_esp: 'Error inesperado al crear usuario',
            });
          }
        }
      );
    });
  }

  async getAll(req, res) {
    DB.connection.then(async () => {
      await Model.find((err, response) => {
        res.send({ status: 'ok', data: response });
      });
    });
  }

  async delete(req, res) {
    await Model.findOneAndDelete({ id: req.params.id }, (err, response) => {
      if (!response) {
        res.status(404).send({
          status: 'not fount',
          msg_ing: 'User not fount',
          msg_esp: 'No se encontro el user',
        });
      } else {
        res.status(201).send({ status: 'ok', data: 'Client delete' });
      }
    })
      .populate('profile')
      .populate('site');
  }

  getOne(req, res) {
    Model.findOne({ id: req.params.id }, async (err, response) => {
      if (!response) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Client not fount',
          msg_esp: 'No se encontro el usuario',
        });
      } else {
        res.status(201).send({ status: 'ok', data: response });
      }
    });
  }

 
}

const model = new ModelController();
export default model;

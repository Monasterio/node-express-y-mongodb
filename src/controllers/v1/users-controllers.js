/* eslint-disable class-methods-use-this */
import bcrypt from 'bcrypt';
import { v4 as uuid4 } from 'uuid';
import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
import DB from '../../database';
import User from '../../models/users';

dotenv.config();

class UserController {
  async changedPassword(req, res) {
    const hash = await bcrypt.hash(req.body.password, 15);
    User.findOneAndUpdate(
      {
        idReset: req.body.idReset,
      },
      {
        password: hash,
        idReset: '',
      },
      { new: true },
      async (err, response) => {
        if (!err) {
          if (response) {
            res.status(200).send({
              status: 'ok',
              data: response,
            });
          } else {
            res.status(404).send({
              status: 'not fount',
              msg_ing: 'User not fount',
              msg_esp: 'No se encontro el usuario',
            });
          }
        } else {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'User not fount',
            msg_esp: 'No se encontro el usuario',
          });
        }
      }
    );
  }

  async resetPassword(req, res) {
    const hash = uuid4();
    const route = 'link por definir';
    const link = route.concat(hash);
    const transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      },
    });
    const user = await User.findOne({ email: req.body.email }).exec();
    if (!user) {
      res.status(404).send({
        status: 'not fount',
        msg_ing: 'Email not fount',
        msg_esp: 'No se encontro el email',
      });
    } else {
      user.idReset = hash;
      user.save();
      const mailOptions = {
        from: process.env.EMAIL_USER,
        to: req.body.email,
        subject: 'Asunto',
        text: link,
        body: 'Test email text',
        html: `<p>Click <a href="${process.env.WEB_HOST}:${process.env.WEB_PORT}/remember-password/${hash}">here</a> to reset your password</p>`,
      };

      transporter.sendMail(mailOptions, (err, mail) => {
        if (err) {
          res.status(500).send({ status: 'error', msg_ing: 'Error send email', msg_esp: 'Error al enviar email' });
        } else {
          res.status(200).send({ status: 'ok', data: hash });
        }
      });
    }
  }

  async logout(req, res) {
    res.status(200).send({
      status: 'ok',
      data: 'user logout',
    });
  }

  async login(req, res) {
    const { email, password } = req.body;
    DB.connection.then(async () => {
      User.findOne({ email }, '+password', async (err, user) => {
        if (user) {
          const isValidte = await bcrypt.compare(password, user.password);
          if (isValidte) {
            const response = {
              id: user.id,
              name: user.name,
              firstLastName: user.firstLastName,
              secondLastName: user.secondLastName,
              site: user.site,
              profile: user.profile,
              zones: user.zones,
              distributors: user.distributors,
              dealers: user.dealers,
            };
            const token = jwt.sign(
              { emai: response.email, id: response.id },
              process.env.JWT_SECRET,
              {
                expiresIn: 60 * 100,
              }
            );
            res.status(200).send({
              status: 'ok',
              data: response,
              token,
              expiresIn: 60 * 100,
            });
          } else {
            res.status(404).send({
              status: 'not fount',
              msg_ing: 'Email or password dont match',
              msg_esp: 'Email o contraseña no coinciden',
            });
          }
        } else {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'User not fount',
            msg_esp: 'No se encontro el usuario',
          });
        }
        if (err) {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'Error inesperado',
            msg_esp: 'Error inesperado',
          });
        }
      })
        .populate('profile')
        .populate('dealers')
        .populate('zones')
        .populate('site')
        .populate('distributors');
    });
  }

  async create(req, res) {
    DB.connection.then(async () => {
      const profile = await Profile.findOne({ id: req.body.profile }).exec();
      const site = await Site.findOne({ id: req.body.site }).exec();
      const hash = await bcrypt.hash('secret', 15);
      const uuid = uuid4();
      const user = new User({
        id: uuid,
        name: req.body.name,
        firstLastName: req.body.firstLastName,
        secondLastName: req.body.secondLastName,
        email: req.body.email,
        password: hash,
        site: site._id,
        profile: profile._id,
      });
      await user.save().then(
        (response) => {
          response
            .populate('profile')
            .populate('site', (err, resp) => {
              console.log('esa es la respuesta', resp);
              res.send({ status: 'ok', data: resp });
            });
        },
        (err) => {
          const error = err.errmsg || err.message;
          if (error.indexOf('email_1') !== -1) {
            res.status(400).json({
              status: 'error',
              msg_ing: 'Email already exists',
              msg_esp: 'Email ya esta registrado',
            });
          }
          else if (error.indexOf('site_1') !== -1) {
            res.status(400).json({
              status: 'error',
              msg_ing: 'Site already exists',
              msg_esp: 'El distribuidor ya posee un usuario asignado',
            });
          } else if (error.indexOf('profile_1') !== -1) {
            res.status(400).json({
              status: 'error',
              msg_ing: 'Distribuitors already exists',
              msg_esp: 'El distribuidor ya posee un usuario asignado',
            });
          } else {
            console.log(error);
            res.status(500).json({
              status: 'error',
              msg_ing: 'Problem with created user',
              msg_esp: 'Error inesperado al crear usuario',
            });
          }
        },
      );
    });
  }

  async getAll(req, res) {
    DB.connection.then(async () => {
      await User.find((err, doc) => {
        res.send({ status: 'ok', data: doc });
      })
        .populate('profile')
        .populate('dealers')
        .populate('zones')
        .populate('site')
        .populate('distributors');
    });
  }

  async deleteUser(req, res) {
    await User.findOneAndDelete({ id: req.params.id }, (err, user) => {
      if (!user) {
        res.status(404).send({
          status: 'not fount',
          msg_ing: 'User not fount',
          msg_esp: 'No se encontro el user',
        });
      } else {
        User.find((_err, response) => {
          res.status(201).send({ status: 'ok', data: response });
        });
      }
    })
      .populate('profile')
      .populate('site');
  }

  getOne(req, res) {
    User.findOne({ id: req.params.id }, async (err, user) => {
      if (!user) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'User not fount',
          msg_esp: 'No se encontro el usuario',
        });
      } else {
        res.status(201).send({ status: 'ok', data: user });
      }
    })
      .populate('profile')
      .populate('dealers')
      .populate('zones')
      .populate('site')
      .populate('distributors');
  }

  updateUser(req, res) {
    DB.connection.then(async () => {
      if (!req.params.id) {
        res.status(404).send({
          status: 'error',
          msg_ing: 'ID is null',
          msg_esp: 'Debe enviar un Id',
        });
      }
      if (!req.body.name) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Name is null',
          msg_esp: 'Debe agregar el nombre',
        });
      }
      if (!req.body.firstLastName) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'First lastName is null',
          msg_esp: 'Debe agregar el primer apellido',
        });
      }
      if (!req.body.secondLastName) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Second lastName is null',
          msg_esp: 'Debe agregar el segundo apellido',
        });
      }
      if (!req.body.email) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Email is null',
          msg_esp: 'Debe agregar el email',
        });
      }
      if (!req.body.site) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Site is null',
          msg_esp: 'Debe agregar el puesto',
        });
      }
      if (!req.body.profile) {
        res.status(404).json({
          status: 'error',
          msg_ing: 'Profile is null',
          msg_esp: 'Debe agregar el perfil',
        });
      }
      User.findOneAndUpdate(
        {
          id: req.params.id,
        },
        {
          name: req.body.name,
          firstLastName: req.body.firstLastName,
          secondLastName: req.body.secondLastName,
          email: req.body.email,
        },
        { new: true },
        (err, response) => {
          if (!err) {
            if (response) {
              res.status(200).send({ status: 'ok', data: response });
            } else {
              res.status(404).send({
                status: 'not fount',
                msg_ing: 'User not fount',
                msg_esp: 'No se encontro la usuario',
              });
            }
          } else {
            const error = err.errmsg || err.message;
            if (error.indexOf('email_1') !== -1) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Email already exists',
                msg_esp: 'Email ya esta registrado',
              });
            } else if (error.indexOf('site_1') !== -1) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Site already exists',
                msg_esp: 'El distribuidor ya posee un usuario asignado',
              });
            } else if (error.indexOf('profile_1') !== -1) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Distribuitors already exists',
                msg_esp: 'El distribuidor ya posee un usuario asignado',
              });
            } else {
              res.status(500).json({
                status: 'error',
                msg_ing: 'Problem with created user',
                msg_esp: 'Error inesperado al crear usuario',
              });
            }
          }
        },
      )
        .populate('profile')
        .populate('site', (err, resp) => {
          res.send({ status: 'ok', data: resp });
        });
    });
  }
}

const user = new UserController();
export default user;

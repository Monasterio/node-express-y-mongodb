/* eslint-disable array-callback-return */
/* eslint-disable class-methods-use-this */
import { v4 as uuid4 } from 'uuid';
import Model from '../../models/products';
import DB from '../../database';

class ModelController {
  async create(req, res) {
    DB.connection.then(async () => {
      const uuid = uuid4();
      const model = new Model({
        id: uuid,
        code: req.body.code,
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
      });
      await model.save().then(
        (response) => {
          res.send({ status: 'ok', data: response });
        },
        (err) => {
          const error = err.errmsg || err.message;
          if (error.indexOf('code_1')) {
            res.status(400).json({
              status: 'error',
              msg_ing: 'Code already exists',
              msg_esp: 'El codigo ya existe',
            });
          } else {
            res.status(500).json({
              status: 'error',
              msg_ing: 'Problem with created zone',
              msg_esp: 'Error inesperado al crear zona',
            });
          }
        }
      );
    });
  }

  async getAll(req, res) {
    DB.connection.then(async () => {
      await Model.find((err, response) => {
        res.send({ status: 'ok', data: response });
      });
    });
  }

  getOne(req, res) {
    DB.connection.then(async () => {
      Model.findOne({ id: req.params.id }, (err, response) => {
        if (response != null) {
          res.send({ status: 'ok', data: response });
        } else {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'Product not fount',
            msg_esp: 'No se encontro el product',
          });
        }
      }).exec();
    });
  }

  async update(req, res) {
    DB.connection.then(async () => {
      Model.findOneAndUpdate(
        {
          id: req.params.id,
        },
        {
          code: req.body.code,
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
        },
        { new: true },
        (err, response) => {
          if (!err) {
            if (response) {
              res.status(200).send({ status: 'ok', data: response });
            } else {
              res.status(404).send({
                status: 'not fount',
                msg_ing: 'Product not fount',
                msg_esp: 'No se el producto',
              });
            }
          } else {
            const error = err.errmsg || err.message;
            if (error.indexOf('code_1')) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Code already exists',
                msg_esp: 'El codigo ya existe',
              });
            }
          }
        }
      );
    });
  }

  async delete(req, res) {
    DB.connection.then(async () => {
      await Model.findOneAndDelete({ id: req.params.id }, (err, response) => {
        if (!response) {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'Product not fount',
            msg_esp: 'No se encontro el product',
          });
        } else {
          Model.find((_err, response2) => {
            res.status(201).send({ status: 'ok', data: response2 });
          });
        }
      });
    });
  }
}

const modelController = new ModelController();
export default modelController;

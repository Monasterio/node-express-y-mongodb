/* eslint-disable array-callback-return */
/* eslint-disable class-methods-use-this */
import { v4 as uuid4 } from 'uuid';
import Model from '../../models/invoices';
import Product from '../../models/products';
import Client from '../../models/clients';
import User from '../../models/users';
import DB from '../../database';
import Email from '../../services/email';

class ModelController {
  async create(req, res) {
    DB.connection.then(async () => {
      const {
        address,
        client,
        products,
        discount,
        creator,
        total,
        subTotal,
      } = req.body;
      const creatorFind = await User.findOne({ id: creator }).exec();
      const productsFind = await Product.find({ id: products }).exec();
      const clientFind = await Client.findOne({
        id: client,
      }).exec();
      if (clientFind && productsFind.length) {
        let subtotal = 0;
        productsFind.map((product) => {
          subtotal += parseInt(product.price);
        });
        const uuid = uuid4();
        const code = uuid4();
        const model = new Model({
          id: uuid,
          code: Math.round(Math.random() * 999999) + Date.now(),
          total,
          subtotal: subTotal,
          creator: creatorFind._id,
          status: 'To pay',
          discount,
          products: productsFind,
          address,
          client: clientFind._id,
        });
        await model.save().then(
          (response) => {
            Email.send(response, clientFind);
            Model.find({})
              .sort({ createdAt: -1 })
              .then((responseFinal) => {
                res.send({ status: 'ok', data: responseFinal });
              });
          },
          (err) => {
            console.log('este es el error', err);
            const error = err.errmsg || err.message;
            if (error.indexOf('code_1')) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Code already exists',
                msg_esp: 'El codigo ya existe',
              });
            } else {
              res.status(500).json({
                status: 'error',
                msg_ing: 'Problem with created zone',
                msg_esp: 'Error inesperado al crear zona',
              });
            }
          }
        );
      } else {
        res.status(404).send({
          status: 'not fount',
          msg_ing: 'Client or products not fount',
          msg_esp: 'No se encontro el cliente o los productos enviados',
        });
      }
    });
  }

  async getAll(req, res) {
    DB.connection.then(async () => {
      const model = await Model.find().sort({ createdAt: -1 }).populate('creator').exec();
      res.send({ status: 'ok', data: model });
    });
  }

  async payInvoice(req, res) {
    DB.connection.then(async () => {
      const model = await Model.findOne({ id: req.params.id }).exec();
      if (model) {
        model.status = 'Payment';
        model.save();
        res.send({ status: 'ok', data: model });
      } else {
        res.status(404).send({
          status: 'not fount',
          msg_ing: 'Invoice not fount',
          msg_esp: 'No se encontro la factura',
        });
      }
    });
  }

  getOne(req, res) {
    DB.connection.then(async () => {
      Model.findOne({ id: req.params.id }, (err, response) => {
        if (response != null) {
          res.send({ status: 'ok', data: response });
        } else {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'Invoices not fount',
            msg_esp: 'No se encontro la factura',
          });
        }
      }).populate('creator')
        .populate('client')
        .exec();
    });
  }

  async update(req, res) {
    DB.connection.then(async () => {
      Model.findOneAndUpdate(
        {
          id: req.params.id,
        },
        {
          code: req.body.code,
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
        },
        { new: true },
        (err, response) => {
          if (!err) {
            if (response) {
              res.status(200).send({ status: 'ok', data: response });
            } else {
              res.status(404).send({
                status: 'not fount',
                msg_ing: 'Product not fount',
                msg_esp: 'No se el producto',
              });
            }
          } else {
            const error = err.errmsg || err.message;
            if (error.indexOf('code_1')) {
              res.status(400).json({
                status: 'error',
                msg_ing: 'Code already exists',
                msg_esp: 'El codigo ya existe',
              });
            }
          }
        }
      );
    });
  }

  async delete(req, res) {
    DB.connection.then(async () => {
      await Model.findOneAndDelete({ id: req.params.id }, (err, response) => {
        if (!response) {
          res.status(404).send({
            status: 'not fount',
            msg_ing: 'Product not fount',
            msg_esp: 'No se encontro el product',
          });
        } else {
          Model.find((_err, response2) => {
            res.status(201).send({ status: 'ok', data: response2 });
          });
        }
      });
    });
  }
}

const modelController = new ModelController();
export default modelController;

/* eslint-disable no-console */
import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

class Database {
  constructor() {
    this.connection = mongoose.connect(
      `mongodb://${process.env.DDB_USER}:${process.env.DDB_PASSWORD}@${
        process.env.DDB_HOST
      }:${process.env.DDB_PORT}/${process.env.DDB_NAME}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    );
    mongoose.set('useCreateIndex', true);
  }
}

const DB = new Database();
module.exports = DB;

/* eslint-disable class-methods-use-this */
import dotenv from 'dotenv';

dotenv.config();

class Validate {
  changedPassword(req, res, next) {
    const { idReset, password, confirm } = req.body;
    let check = true;
    if (!idReset) {
      check = false;
      res.status(404).send({
        status: 'error',
        msg_ing: 'Id is null',
        msg_esp: 'Id es nulo',
      });
    }
    if (!password) {
      check = false;
      res.status(404).send({
        status: 'error',
        msg_ing: 'Password is null',
        msg_esp: 'La contraseña esta vacia',
      });
    }
    if (!confirm) {
      check = false;
      res.status(404).send({
        status: 'error',
        msg_ing: 'password confirm is null',
        msg_esp: 'La confirmacion de la contraseña esta vacia',
      });
    }
    if (password !== confirm) {
      check = false;
      res.status(404).send({
        status: 'error',
        msg_ing: 'password and confirm dont match',
        msg_esp: 'La contraseña y confirmacion no coinciden',
      });
    }
    if (check === true) {
      next();
    }
  }

  create(req, res, next) {
    if (!req.body.name) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Name is null',
        msg_esp: 'Debe agregar el nombre',
      });
    }
    if (!req.body.firstLastName) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'First lastName is null',
        msg_esp: 'Debe agregar el primer apellido',
      });
    }
    if (!req.body.secondLastName) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Second lastName is null',
        msg_esp: 'Debe agregar el segundo apellido',
      });
    }
    if (!req.body.email) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Email is null',
        msg_esp: 'Debe agregar el email',
      });
    }
    if (!req.body.site) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Site is null',
        msg_esp: 'Debe agregar el puesto',
      });
    }
    if (!req.body.profile) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Profile is null',
        msg_esp: 'Debe agregar el perfil',
      });
    }
    next();
  }
}

const validate = new Validate();
export default validate;

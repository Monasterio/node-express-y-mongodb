/* eslint-disable class-methods-use-this */
import dotenv from 'dotenv';

dotenv.config();

class Validate {
  body(req, res, next) {
    const { code, name, price, description } = req.body;

    if (!code) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Code is null',
        msg_esp: 'Debe agregar el codigo del producto',
      });
    }
    if (!name) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Name is null',
        msg_esp: 'Debe agregar el nombre del producto',
      });
    }
    if (!price) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Price is null',
        msg_esp: 'Debe agregar el precio del producto',
      });
    }
    if (!description) {
      res.status(404).json({
        status: 'error',
        msg_ing: 'Description is null',
        msg_esp: 'Debe agregar la descripción del producto',
      });
    }
    next();
  }
}

const validate = new Validate();
export default validate;

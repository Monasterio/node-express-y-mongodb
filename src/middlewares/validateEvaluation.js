/* eslint-disable class-methods-use-this */
import dotenv from 'dotenv';

dotenv.config();

class ValidateEvaluation {
  body(req, res, next) {
    let check = true;
    if (!req.body.code) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'code is null',
        msg_esp: 'Debe agregar el codigo',
      });
    } else if (!req.body.name) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Name is null',
        msg_esp: 'Debe agregar el nombre',
      });
    } else if (!req.body.evaluate) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Evaluate is null',
        msg_esp: 'Debe agregar la Evaluacion',
      });
    } else if (!req.body.process) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Process is null',
        msg_esp: 'Debe agregar el proceso',
      });
    } else if (!req.body.question) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Question is null',
        msg_esp: 'Debe agregar una pregunta',
      });
    } else if (!req.body.evaluator) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Email is null',
        msg_esp: 'Debe agregar el email',
      });
    } else if (!req.body.responsable) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Responsable is null',
        msg_esp: 'Debe agregar el responsable',
      });
    } else if (!req.body.kpi) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Kpi is null',
        msg_esp: 'Debe agregar el kpi',
      });
    } else if (req.body.requirement.length === 0) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Requirement is null',
        msg_esp: 'Debe agregar los requerimientos',
      });
    } else if (!req.body.user) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'User is null',
        msg_esp: 'Debe agregar un Usuario',
      });
    } else {
      next();
    }
  }

  update(req, res, next) {
    let check = true;
    if (req.params.id === '{id}') {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Id is null',
        msg_esp: 'Debe agregar el id',
      });
    } else if (!req.body.code) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'code is null',
        msg_esp: 'Debe agregar el codigo',
      });
    } else if (!req.body.name) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Name is null',
        msg_esp: 'Debe agregar el nombre',
      });
    } else if (!req.body.question) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Question is null',
        msg_esp: 'Debe agregar una pregunta',
      });
    } else if (!req.body.evaluator) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Email is null',
        msg_esp: 'Debe agregar el email',
      });
    } else if (!req.body.responsable) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Responsable is null',
        msg_esp: 'Debe agregar el responsable',
      });
    } else if (!req.body.kpi) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Kpi is null',
        msg_esp: 'Debe agregar el kpi',
      });
    } else if (req.body.requirement.length === 0) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'Requirement is null',
        msg_esp: 'Debe agregar los requerimientos',
      });
    } else if (!req.body.user) {
      check = false;
      res.status(404).json({
        status: 'error',
        msg_ing: 'User is null',
        msg_esp: 'Debe agregar un Usuario',
      });
    } else {
      next();
    }
  }
}

const validate = new ValidateEvaluation();
export default validate;

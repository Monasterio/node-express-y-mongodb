/* eslint-disable class-methods-use-this */
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

class Authenticacion {
  validateUser(req, res, next) {
    jwt.verify(req.headers.token, process.env.JWT_SECRET, (err, verify) => {
      if (err) {
        res.status(401).send({
          status: 'error',
          msg_ing: 'User Not authorized',
          msg_esp: 'Usuario no autorizado',
        });
      } else {
        req.dataValidate = {
          id: verify.id,
          _id: verify._id,
          user: verify.user,
          profile: verify.code,
        };
        next();
      }
    });
  }
}

const authenticacion = new Authenticacion();
export default authenticacion;

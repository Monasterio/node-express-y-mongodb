/* eslint-disable import/prefer-default-export */
import { productsSeeder } from './products-seeder';
import { userSeeder } from './user-seeder';

export const Seeder = [];
Seeder.push(productsSeeder);
Seeder.push(userSeeder);

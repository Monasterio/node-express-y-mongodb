// eslint-disable-next-line import/prefer-default-export
export const userSeeder = {
  model: 'User',
  documents: [
    {
      _id: '5f28141c977edd1e3679c2cb',
      id: '401c2ed6-d8d1-441d-b5eb-710775e59991',
      name: 'Owner',
      lastName: 'Owner Lastname',
      email: 'owner@gmail.com',
      password: '$2b$15$XMHnlD4Id1LV5DmzBbihyeI1.5nwm9uIZUuEqsr327i7M2fU5S4MS',
      type: 'Owner',
    },
    {
      _id: '5f28141c977edd1e3679c1cb',
      id: '401c2ed6-d8d1-441d-b5eb-710775e59992',
      name: 'Client',
      lastName: 'Client Lastname',
      email: 'client@gmail.com',
      password: '$2b$15$XMHnlD4Id1LV5DmzBbihyeI1.5nwm9uIZUuEqsr327i7M2fU5S4MS',
      type: 'Client',
    },
  ],
};

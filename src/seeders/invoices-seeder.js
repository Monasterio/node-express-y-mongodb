// eslint-disable-next-line import/prefer-default-export
export const invoiceSeeder = {
  model: 'Invoice',
  documents: [
    {
      _id: '5f25bc5a54211344f6a5ad35',
      id: 'deb65c41-a25a-42e8-82e3-7a12fba53d0f',
      code: '01site',
      description: 'Site 1 description',
    },
    {
      _id: '5f25bc8354211344f6a5ad36',
      id: '6014a9c7-e1ad-49a2-8815-e87c1c1568d4',
      code: '02site',
      description: 'Site 2 description',
    },
    {
      _id: '5f25bc9f54211344f6a5ad37',
      id: 'f297303e-c7a6-4526-8ca2-9c29eabb37ff',
      code: '03site',
      description: 'Site 3 description',
    },
    {
      _id: '5f25bcc454211344f6a5ad38',
      id: 'fdae7adf-b8a5-40b5-a3a7-02d44e463b8f',
      code: '04site',
      description: 'Site 4 description',
    },
  ],
};

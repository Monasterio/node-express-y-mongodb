// eslint-disable-next-line import/prefer-default-export
export const productsSeeder = {
  model: 'Product',
  documents: [
    {
      _id: '5f25c06e1895554644b5b83d',
      id: '13f8453a-6c39-477a-9311-554a6c0932f0',
      name: 'Zapatos',
      price: 100,
      code: '01product',
      description: 'product 1 description',
    },
    {
      _id: '5f25c0901895554644b5b83e',
      id: '4281e18a-6225-444f-83d7-b51384cc12f5',
      name: 'Pantalon',
      price: 200,
      code: '02product',
      description: 'product 2 description',
    },
    {
      _id: '5f25c0a91895554644b5b83f',
      id: '8389ead8-a83e-496e-a901-59b194d83c80',
      name: 'Camisa',
      price: 150,
      code: '03product',
      description: 'product 3 description',
    },
    {
      _id: '5f25c0c71895554644b5b840',
      id: '93fab959-2120-4961-80d6-29634a2622c9',
      name: 'Medias',
      price: 50,
      code: '04product',
      description: 'product 4 description',
    },
  ],
};

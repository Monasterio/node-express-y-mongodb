/* eslint-disable class-methods-use-this */
import nodemailer from 'nodemailer';
import dotenv from 'dotenv';

class EmailServices {
  async send(invoice, client) {
    const transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      }
    });

    const content = `
    <!doctype html>
        <html>
          <head> 
                <meta charset="utf-8">
                <title>PDF Result Template</title>
                <style>
                  h4{
                    margin-top: .5rem;
                    margin-bottom: .5rem;
                    font-size: 12px;
                  }
                  .head{
                    color: rgba(0, 0, 0, 0.65);
                    font-size: 14px;
                    font-variant: tabular-nums;
                    line-height: 1.5;
                    list-style: none;
                    font-feature-settings: 'tnum', "tnum";
                  }
                  .head th{
                    background: #f15a22;
                    color: #fff;
                    font-weight: 800;
                    text-align: center;
                  }
                  h1{
                    font-size: 18px;
                    text-align: center;
                    margin-top: 1rem;
                  }
                  .container{
                    padding: 2.5rem;
                  }
                </style>    
            </head>
            <body>
                <div class="container">
                  <h1>Notificacion de facturación</h1>
                  <h4>Estimado ${client.name} ${client.lastName}</h4>
                  <h4>Notificamos que la factura Numero: ${invoice.code} le fue emitida, gracias por preferirnos</h4>
                  <table class="egt">
                  <p> Por favor ingrese <a href=${process.env.WEB_HOST}/payment/${invoice.id}>aqui</a> para cancelar factura</p>
                  </table>
                </div>
              </body>
        </html>
    `;

    const mailOptions = {
      from: process.env.EMAIL_USER,
      to: client.email,
      subject: 'Notificacion de facturación',
      body: 'Facturacion',
      html: content,
    };
    transporter.sendMail(mailOptions, (errr, mail) => {
      if (mail) {
        return 1;
      }
      if (errr) {
        console.log('err', errr)
        return errr;
      }
    });
  }
}

const emailServices = new EmailServices();
export default emailServices;
